package com.imaginaformacion.tema6ej1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;


/**
 * Created by manel on 19/09/2015.
 */
public class MyDialogFragment extends DialogFragment {

    public static final String ARG_TITLE="title";
    public static final String ARG_DESC="desc";


    public static MyDialogFragment newInstance(Curso curso) {
        MyDialogFragment fragment=new MyDialogFragment();
        Bundle args=new Bundle();
        args.putString(ARG_TITLE,curso.getTitulo());
        args.putString(ARG_DESC, curso.getDescripcion());
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        String desc = getArguments().getString(ARG_DESC);
        String title = getArguments().getString(ARG_TITLE);


        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(desc)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                    }
                }).create();

    }


}
